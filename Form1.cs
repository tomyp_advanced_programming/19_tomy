﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Threading.Tasks;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void lg_Click(object sender, EventArgs e)
        {
            if (File.ReadAllLines(@"C:\Users\magshimim\source\repos\WindowsFormsApp1\WindowsFormsApp1\Users.txt").Select(x => x.Trim()).Contains(nametxt.Text.Trim() + "," + passtxt.Text.Trim()))
            {
                string path = nametxt.Text.Trim() + "DB.txt";
                StreamWriter sw = (File.Exists(path)) ? File.AppendText(path) : File.CreateText(path);
                sw.Close();
                MessageBox.Show("correct HAVE FUN");
                calendar cal = new calendar(nametxt.Text.Trim());
                cal.Show();
            }
            else
            {
                MessageBox.Show("Not Correct OOPS TRY LATER AGAIN");
            }
        }

        private void cn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void nametxt_TextChanged(object sender, EventArgs e)
        {
            Random randomGen = new Random();
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor randomColorName = names[randomGen.Next(names.Length)];
            Color randomColor = Color.FromKnownColor(randomColorName);

            nametxt.BackColor = randomColor;
        }

        private void passtxt_TextChanged(object sender, EventArgs e)
        {
            Random randomGen = new Random();
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor randomColorName = names[randomGen.Next(names.Length)];
            Color randomColor = Color.FromKnownColor(randomColorName);

            passtxt.BackColor = randomColor;
        }

    }
}