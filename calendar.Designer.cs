﻿namespace WindowsFormsApp1
{
    partial class calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mc1 = new System.Windows.Forms.MonthCalendar();
            this.pcal = new System.Windows.Forms.Label();
            this.newuser = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.addtodata = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // mc1
            // 
            this.mc1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.mc1.Location = new System.Drawing.Point(428, 18);
            this.mc1.Name = "mc1";
            this.mc1.TabIndex = 0;
            this.mc1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // pcal
            // 
            this.pcal.AutoSize = true;
            this.pcal.Location = new System.Drawing.Point(442, 189);
            this.pcal.Name = "pcal";
            this.pcal.Size = new System.Drawing.Size(0, 13);
            this.pcal.TabIndex = 1;
            // 
            // newuser
            // 
            this.newuser.Location = new System.Drawing.Point(147, 42);
            this.newuser.Multiline = true;
            this.newuser.Name = "newuser";
            this.newuser.Size = new System.Drawing.Size(200, 61);
            this.newuser.TabIndex = 2;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(147, 132);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // addtodata
            // 
            this.addtodata.Location = new System.Drawing.Point(147, 158);
            this.addtodata.Name = "addtodata";
            this.addtodata.Size = new System.Drawing.Size(118, 44);
            this.addtodata.TabIndex = 4;
            this.addtodata.Text = "Add to database";
            this.addtodata.UseVisualStyleBackColor = true;
            this.addtodata.Click += new System.EventHandler(this.addtodata_Click);
            // 
            // calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 297);
            this.Controls.Add(this.addtodata);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.newuser);
            this.Controls.Add(this.pcal);
            this.Controls.Add(this.mc1);
            this.Name = "calendar";
            this.Text = "calendar";
            this.Load += new System.EventHandler(this.calendar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar mc1;
        private System.Windows.Forms.Label pcal;
        private System.Windows.Forms.TextBox newuser;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button addtodata;
    }
}